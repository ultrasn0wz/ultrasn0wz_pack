# ultrasn0wz Modpack Installation
![LATINA](https://images6.alphacoders.com/102/1022922.jpg)

## Vorbereitung
1. Minecraft [runterladen](https://www.minecraft.net/en-us/download/alternative/) & installieren
2. Alle Java Versionen älter als 14 deinstallieren
3. Java 14 JRE x64 [runterladen](https://adoptopenjdk.net/releases.html) & installieren

## Moddable Minecraft Instanz erstellen
1. Minecraft Launcher öffnen, eine 1.7.10 Installation erstellen und einmal starten; danach alles beenden
2. MinecraftForge [runterladen](https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.4.1614-1.7.10/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar) & starten
3. *Install client* bestätigen und schließen wenn fertig
4. Minecraft Launcher starten und die neue *1.7.10-Forge10.13.4.1614-1.7.10* Instanz bearbeiten (*More Options* aufklappen)
5. Wo auch immer ihr das Modpack installiert haben wollt einen Ordner erstellen, darin noch einen Unterordner *instance* und *GAME DIRECTORY* auf *instance* zeigen lassen
`Bsp.: ..\BestesModpack\instance`
6. Aus den bereitgestellten Dateien, die **jre.7z** in den neu erstellten Ordner ohne Unterordner entpacken
`Bsp.: ..\BestesModpack\jre`
8. In der Minecraft Instanz *JAVA EXECUTABLE* auf **bin\javaw.exe** der ausgepackten jre stellen
`Bsp.: ..\BestesModpack\jre\bin\javaw.exe`
9. In *JVM ARGUMENTS* `-Xmx3G` zu der gewünschten Menge an RAM ändern welche Minecraft verwenden darf (Empfohlen: 4-5GB, 3GB wenn nur 8GB RAM)
`Bsp.: -Xmx4G`
10. Instanz speichern und einmal starten

## Modpack installieren
1. **ultrasn0wz_modpack.7z** in den *instance* Ordner ohne Unterordner entpacken
```
Bsp.: ..\BestesModpack\instance\config
Bsp.: ..\BestesModpack\instance\mods
Bsp.: ..\BestesModpack\instance\[...]
```
2. Starten (3min Tee bei gutem CPU)
3. Spielen
