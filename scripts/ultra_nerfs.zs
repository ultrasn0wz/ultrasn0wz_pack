import mods.MTUtilsGT;

//MC
furnace.remove(<ore:ingotIron>); //galacticraft
furnace.remove(<ore:ingotBronze>); //furnace
furnace.remove(<ore:ingotTitanium>);//galacticraft
furnace.remove(<ore:ingotAluminum>);//???
furnace.remove(<ore:ingotDesh>);//galacticraft
furnace.remove(<ore:ingotMeteoricIron>);//galacticraft

recipes.remove(<atum:tile.furnaceIdle>);
recipes.addShaped(<atum:tile.furnaceIdle>, [[<atum:tile.cobble>, <atum:tile.cobble>, <atum:tile.cobble>], [<atum:tile.cobble>, <ore:craftingFirestarter>, <atum:tile.cobble>], [<atum:tile.cobble>, <atum:tile.cobble>, <atum:tile.cobble>]]);

furnace.remove(<ProjRed|Core:projectred.core.part>);
recipes.addShapeless(<ProjRed|Core:projectred.core.part>, [<ore:plateStone>, <ore:craftingToolFile>]);

//chisel

recipes.remove(<chisel:chisel>);
recipes.remove(<chisel:diamondChisel>);
recipes.remove(<chisel:obsidianChisel>);
recipes.addShaped(<chisel:chisel>, [[<ore:toolHeadChiselAnyIron>], [<ore:stickWood>], [<ore:stickWood>]]);
recipes.addShaped(<chisel:diamondChisel>, [[<ore:toolHeadChiselDiamond>], [<ore:stickWood>], [<ore:stickWood>]]);
recipes.addShaped(<chisel:obsidianChisel>, [[null, <ore:plateObsidian>], [<ore:stickObsidian>, null]]);

//EnderCompass

recipes.remove(<endercompass:ender_compass>);
recipes.addShaped(<endercompass:ender_compass>, [[null, <ore:gemEnderPearl>, null], [<ore:gemEnderEye>, <EnderIO:itemXpTransfer>, <ore:gemEnderEye>], [null, <ore:gemEnderPearl>, null]]);

//SpiceofLive

recipes.remove(<SpiceOfLife:lunchbox>);
recipes.remove(<SpiceOfLife:lunchbag>);
recipes.remove(<SpiceOfLife:bookfoodjournal>);
recipes.addShapeless(<SpiceOfLife:lunchbox>, [<minecraft:wooden_button>, <ore:casingSmallIron>]);
recipes.addShaped(<SpiceOfLife:lunchbag>, [[null, <ore:paperEmpty>, null], [<ore:paperEmpty>, null, <ore:paperEmpty>], [<ore:paperEmpty>, <ore:paperEmpty>, <ore:paperEmpty>]]);
recipes.addShapeless(<SpiceOfLife:bookfoodjournal>, [<ore:bookEmpty>, <ore:listAllseed>]);

//Railcraft

recipes.remove(<Railcraft:tool.magnifying.glass>);
recipes.addShaped(<Railcraft:tool.magnifying.glass>, [[null, <ore:lensGlass>], [<ore:stickAnyWood>, null]]);

//SSTOW

recipes.remove(<SSTOW:sstow_materials:4>);
recipes.remove(<SSTOW:sstow_forge>);
recipes.remove(<SSTOW:sstow_soul_spade>);
recipes.remove(<SSTOW:sstow_soul_hoe>);
recipes.remove(<SSTOW:sstow_soul_axe>);
recipes.remove(<SSTOW:sstow_soul_pickaxe>);
recipes.remove(<SSTOW:sstow_soul_sword>);
recipes.addShapeless(<SSTOW:sstow_materials:4>, [<ore:dustGlowstone>, <ore:dustVile>, <ore:ingotSoularium>]);
recipes.addShaped(<SSTOW:sstow_forge>, [[<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>], [<ore:cobblestone>, <ore:essenceCorrupted>, <ore:cobblestone>], [<runicdungeons:tile.compressedObsidian>, <IC2:blockMachine:1>, <runicdungeons:tile.compressedObsidian>]]);

//ProjectBlue

recipes.remove(<ProjectBlue:rednetAdaptor>);
recipes.addShaped(<ProjectBlue:rednetAdaptor>, [[<ore:gemLapis>, <ore:sheetPlastic>, <ore:gemLapis>], [<ProjRed|Core:projectred.core.part:3>, <ore:dustRedstone>, <ore:cableRedNet>], [<ore:gemLapis>, <ore:sheetPlastic>, <ore:gemLapis>]]);

//EnderIO
recipes.remove(<EnderIO:itemOCConduit>);
recipes.remove(<EnderIO:itemBasicCapacitor>);
recipes.remove(<EnderIO:itemMaterial:6>);
recipes.remove(<EnderIO:blockEnchanter>);
recipes.remove(<EnderIO:blockCapBank:2>);
recipes.remove(<EnderIO:blockSolarPanel>);
recipes.remove(<EnderIO:blockSolarPanel:1>);
recipes.addShaped(<EnderIO:itemOCConduit> * 4, [[<ore:itemConduitBinder>, <ore:itemConduitBinder>, <ore:itemConduitBinder>], [<ore:oc:cable>, <ore:ingotRedstoneAlloy>, <ore:oc:cable>], [<ore:itemConduitBinder>, <ore:itemConduitBinder>, <ore:itemConduitBinder>]]);
recipes.addShaped(<EnderIO:itemBasicCapacitor>, [[null, <ore:nuggetGold>, <ore:dustRedstone>], [<ore:nuggetGold>, <ore:wireCopper>, <ore:nuggetGold>], [<ore:dustRedstone>, <ore:nuggetGold>, null]]);
recipes.addShaped(<EnderIO:itemMaterial:6>, [[<ore:dustSmallPhasedGold>, <ore:dustSmallPhasedGold>, <ore:dustSmallPhasedGold>], [<ore:dustSmallPhasedGold>, <ore:gemEmerald>, <ore:dustSmallPhasedGold>], [<ore:dustSmallPhasedGold>, <ore:dustSmallPhasedGold>, <ore:dustSmallPhasedGold>]]);
recipes.addShaped(<EnderIO:blockEnchanter>, [[<ore:ingotDarkSteel>, <minecraft:enchanting_table>, <ore:ingotDarkSteel>], [null, <ore:ingotDarkSteel>, null], [<ore:ingotDarkSteel>, <ore:ingotDarkSteel>, <ore:ingotDarkSteel>]]);
recipes.addShaped(<EnderIO:blockCapBank:2>, [[<ore:ingotElectricalSteel>, <EnderIO:itemBasicCapacitor:1>, <ore:ingotElectricalSteel>], [<EnderIO:itemBasicCapacitor:1>, <ore:redstoneCrystal>, <EnderIO:itemBasicCapacitor:1>], [<ore:ingotElectricalSteel>, <EnderIO:itemBasicCapacitor:1>, <ore:ingotElectricalSteel>]]);
recipes.addShaped(<EnderIO:blockCapBank:2>, [[<ore:ingotElectricalSteel>, <EnderIO:itemBasicCapacitor:1>, <ore:ingotElectricalSteel>], [<EnderIO:itemBasicCapacitor:1>, <EnderIO:blockCapBank:1>, <EnderIO:itemBasicCapacitor:1>], [<ore:ingotElectricalSteel>, <EnderIO:itemBasicCapacitor:1>, <ore:ingotElectricalSteel>]]);
recipes.addShaped(<EnderIO:blockSolarPanel>, [[<ore:plateEnergeticAlloy>, <ore:blockGlassHardened>, <ore:plateEnergeticAlloy>], [<ore:plateGemSilicon>, <ore:itemPulsatingCrystal>, <ore:plateGemSilicon>], [<ore:plateElectricalSteel>, <EnderIO:itemBasicCapacitor>, <ore:plateElectricalSteel>]]);
recipes.addShaped(<EnderIO:blockSolarPanel:1>, [[<ore:plateVibrantAlloy>, <EnderIO:blockFusedQuartz:2>, <ore:plateVibrantAlloy>], [<ore:plateGemSilicon>, <ore:itemVibrantCrystal>, <ore:plateGemSilicon>], [<ore:platePulsatingIron>, <EnderIO:itemBasicCapacitor:1>, <ore:platePulsatingIron>]]);

//Chicken Chunks
recipes.remove(<ChickenChunks:chickenChunkLoader>);
recipes.addShaped(<ChickenChunks:chickenChunkLoader>, [[null, <minecraft:ender_pearl>, null], [<ore:ingotGold>, <ore:ingotGold>, <ore:ingotGold>], [<ore:ingotGold>, <Railcraft:machine.alpha>, <ore:ingotGold>]]);

//MFR
recipes.remove(<MineFactoryReloaded:machine.1:3>);
recipes.addShaped(<MineFactoryReloaded:machine.1:3>, [[<ore:sheetPlastic>, <gregtech:gt.multitileentity:18203>, <ore:sheetPlastic>], [<minecraft:ender_eye>, <gregtech:gt.multitileentity:32000>, <minecraft:ender_eye>], [<ore:sheetPlastic>, <EnderIO:blockReinforcedObsidian>, <ore:sheetPlastic>]]);

//Forestry

recipes.remove(<Forestry:wrench>);
recipes.remove(<Forestry:sturdyMachine>);
recipes.addShaped(<Forestry:wrench>, [[<ore:ingotBronze>, null, <ore:ingotBronze>], [null, <ore:gearGtBronze>, null], [null, <ore:ingotBronze>, null]]);
recipes.addShaped(<Forestry:sturdyMachine>, [[<ore:craftingToolFile>, <ore:casingMachineBronze>, <ore:craftingToolScrewdriver>]]);

//GalacticraftCore

recipes.remove(<GalacticraftCore:item.null>);
recipes.remove(<GalacticraftCore:tile.machine2:4>);
recipes.addShapeless(<GalacticraftCore:item.null> * 3, [<ore:crushedMeteoricIron>]);
recipes.addShaped(<GalacticraftCore:tile.machine2:4>, [[<ore:ingotAluminium>, <ore:oc:materialALU>, <ore:ingotAluminium>], [<ore:ingotAluminium>, <IC2:blockMachine:1>, <ore:ingotAluminium>], [<GalacticraftCore:tile.aluminumWire:1>, <ore:blockRedstone>, <GalacticraftCore:tile.aluminumWire:1>]]);

//OpenBlocks

recipes.remove(<OpenBlocks:autoenchantmenttable>);
recipes.addShaped(<OpenBlocks:autoenchantmenttable>, [[<ore:ingotIron>, <ore:gearElectrum>, <ore:ingotIron>], [<ore:ingotIron>, <minecraft:enchanting_table>, <ore:ingotIron>], [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>]]);

//PMP

recipes.remove(<plantmegapack:wallBracketIronCurl>);
recipes.remove(<plantmegapack:wallBracketGoldCurl>);
recipes.addShaped(<plantmegapack:wallBracketIronCurl>, [[<ore:stickLongIron>, <ore:stickLongIron>], [null, <ore:stickLongIron>]]);
recipes.addShaped(<plantmegapack:wallBracketGoldCurl>, [[<ore:stickLongGold>, <ore:stickLongGold>], [null, <ore:stickLongGold>]]);

//Mystcraft

recipes.remove(<Mystcraft:BlockBookstand>);
recipes.addShaped(<Mystcraft:BlockBookstand>, [[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>], [<ore:stickWood>], [<ore:slabWood>]]);

//Atum

recipes.remove(<atum:item.scarab>);
recipes.addShaped(<atum:item.scarab>, [[<ore:foilGold>, <minecraft:diamond>, <ore:foilGold>], [<ore:foilGold>, <TwilightForest:tile.TFFirefly>, <ore:foilGold>], [<ore:foilGold>, <ore:foilGold>, <ore:foilGold>]]);


//runicdungeons
recipes.remove(<runicdungeons:item.basicBelt>);
recipes.remove(<runicdungeons:item.advancedBelt>);
recipes.remove(<runicdungeons:item.flyBelt>);
recipes.addShaped(<runicdungeons:item.flyBelt>, [[<OpenBlocks:generic:5>, <xreliquary:angelic_feather>, <OpenBlocks:generic:5>], [<xreliquary:angelic_feather>, <runicdungeons:item.advancedBelt>, <xreliquary:angelic_feather>], [<OpenBlocks:generic:5>, <ore:blockGemInfusedAir>, <OpenBlocks:generic:5>]]);
recipes.addShaped(<runicdungeons:item.advancedBelt>, [[<OpenBlocks:generic:5>, <arsmagica2:manaPotionBundle:1027>, <OpenBlocks:generic:5>], [<arsmagica2:manaPotionBundle:1027>, <runicdungeons:item.basicBelt>, <arsmagica2:manaPotionBundle:1027>], [<OpenBlocks:generic:5>, <ore:gemLegendaryNetherStar>, <OpenBlocks:generic:5>]]);
recipes.addShaped(<runicdungeons:item.basicBelt>, [[<OpenBlocks:generic:5>, <arsmagica2:liquidEssenceBottle>, <OpenBlocks:generic:5>], [<arsmagica2:itemOre:3>, <Thaumcraft:ItemBaubleBlanks:2>, <arsmagica2:itemOre:3>], [<OpenBlocks:generic:5>, <arsmagica2:deficitCrystal>, <OpenBlocks:generic:5>]]);

//gregtech - needed for runicdungeons belt ------ first one should work, but doesn't
//MTUtilsGT.addCustomRecipe("gt.recipe.autoclave", true, 128, 128, [1280], [<gregtech:gt.meta.dust:8320>*8], [<liquid:chargedmatter>*2424], [<liquid:neutralmatter>*1337], [<gregtech:gt.meta.gemLegendary:8320>]);
//MTUtilsGT.addCustomRecipe(<ore:gemLegendaryNetherStar>, <ore:dustNetherStar>, <liquid:chargedmatter> * 2424, 9000, 2000, 32);
//MTUtilsGT.addCustomRecipe("gt.recipe.canner", false, 128, 128, [10000], [<minecraft:cobblestone>], [<liquid:soda>*500], [<liquid:water>*500], [<minecraft:diamond>]);

//ThermalFoundation

recipes.remove(<ThermalFoundation:material:512>);
recipes.addShapeless(<ThermalFoundation:material:512> * 2, [<ore:blockDustSulfur>, <ore:dustBlaze>, <ore:dustRedstone>, <ore:dustHydratedCoal>]);
recipes.addShapeless(<ThermalFoundation:material:76> * 2, [<ore:dustEnderium>, <ore:dustPyrotheum>, <ore:dustEnderium>]);
recipes.addShapeless(<ThermalFoundation:material:76> * 2, [<ore:ingotEnderiumBase>, <ore:bucketPyrotheum>.giveBack(<minecraft:bucket>), <ore:bucketEnder>.giveBack(<minecraft:bucket>), <ore:ingotEnderiumBase>]);
recipes.addShapeless(<ThermalFoundation:material:76>, [<ore:ingotEnderiumBase>, <ore:dustPyrotheum>, <ore:dustEnderPearl>, <ore:ingotEnderiumBase>]);

//Thaumcraft

recipes.remove(<Thaumcraft:ItemBaubleBlanks:2>);
recipes.addShaped(<Thaumcraft:ItemBaubleBlanks:2>, [[<minecraft:leather>, <minecraft:leather>, <minecraft:leather>], [<minecraft:leather>, <OpenBlocks:generic:5>, <minecraft:leather>], [<minecraft:leather>, <ore:plateDenseGold>, <minecraft:leather>]]);

//WR-CBE|Core

recipes.remove(<WR-CBE|Core:stoneBowl>);
recipes.addShaped(<WR-CBE|Core:stoneBowl>, [[<ore:craftingToolFile>], [<minecraft:stone_slab>]]);

//ProjRed

recipes.remove(<ProjRed|Core:projectred.core.part:43>);
recipes.remove(<ProjRed|Core:projectred.core.part:42>);
recipes.addShaped(<ProjRed|Core:projectred.core.part:43>, [[<ore:dustGlowstone>, <ore:dustGlowstone>, <ore:dustGlowstone>], [<ore:dustGlowstone>, <ore:itemSilicon>, <ore:dustGlowstone>], [<ore:dustGlowstone>, <ore:dustGlowstone>, <ore:dustGlowstone>]]);
recipes.addShaped(<ProjRed|Core:projectred.core.part:42>, [[<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>], [<ore:dustRedstone>, <ore:itemSilicon>, <ore:dustRedstone>], [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>]]);

//Buildcraft

recipes.remove(<BuildCraft|Builders:machineBlock>);
recipes.addShaped(<BuildCraft|Builders:machineBlock>, [[<ore:gearGtDarkSteel>, <gregtech:gt.multitileentity:11023>, <ore:gearGtDarkSteel>], [<gregtech:gt.multiitem.technological:30383>, <gregtech:gt.multiitem.randomtools:8008>, <gregtech:gt.multiitem.technological:30383>], [<ore:gearDiamond>, <ore:toolHeadDrillTitan>, <ore:gearDiamond>]]);

//ExtraUtilities

recipes.remove(<ExtraUtilities:timer>);
recipes.remove(<ExtraUtilities:curtains>);
recipes.addShaped(<ExtraUtilities:timer>, [[<ore:stoneSmooth>, <ProjRed|Transmission:projectred.transmission.wire>, <ore:stoneSmooth>], [<ProjRed|Transmission:projectred.transmission.wire>, <ProjRed|Integration:projectred.integration.gate:17>, <ProjRed|Transmission:projectred.transmission.wire>], [<ore:stoneSmooth>, <ProjRed|Transmission:projectred.transmission.wire>, <ore:stoneSmooth>]]);
//recipes.addShaped(<ExtraUtilities:sound_muffler:1>, [[<ore:blockWool>, <ore:blockWool>, <ore:blockWool>], [<ore:blockWool>, <gregtech:gt.metaitem.01:32727>, <ore:blockWool>], [<ore:blockWool>, <weather2:TornadoSensor>, <ore:blockWool>]]);
//recipes.addShaped(<ExtraUtilities:sound_muffler>, [[<ore:blockWool>, <ore:blockWool>, <ore:blockWool>], [<ore:blockWool>, <gregtech:gt.metaitem.01:32727>, <ore:blockWool>], [<ore:blockWool>, <ore:blockWool>, <ore:blockWool>]]);
recipes.addShaped(<ExtraUtilities:curtains> * 8, [[<minecraft:carpet:*>, <minecraft:carpet:*>], [<minecraft:carpet:*>, <minecraft:carpet:*>], [<minecraft:carpet:*>, <minecraft:carpet:*>]]);

//gendustry

recipes.remove(<gendustry:Labware>);
recipes.remove(<gendustry:IndustrialApiary>);
recipes.remove(<gendustry:PowerModule>);
recipes.addShaped(<gendustry:Labware> * 4, [[<Railcraft:tool.magnifying.glass>, null, <Forestry:pipette>], [null, <ore:plateGemTinyDiamond>, null], [<MineFactoryReloaded:syringe.empty>, null, <ExtraBees:misc>]]);
recipes.addShaped(<gendustry:IndustrialApiary>, [[<ore:blockGlass>, <gendustry:BeeReceptacle>, <ore:blockGlass>], [<ore:blockGlass>, <Forestry:sturdyMachine>, <ore:blockGlass>], [<ore:gearBronze>, <gendustry:PowerModule>, <ore:gearBronze>]]);
recipes.addShaped(<gendustry:PowerModule>, [[<ore:gearBronze>, <ore:plateGold>, <ore:gearBronze>], [<ore:ingotRedstoneAlloy>, <BuildCraft|Transport:pipePowerAdapter>, <ore:ingotRedstoneAlloy>], [<ore:gearBronze>, <ore:plateGold>, <ore:gearBronze>]]);

mods.buildcraft.AssemblyTable.removeRecipe(<gendustry:EnvProcessor>, [<minecraft:diamond>*2, <BuildCraft|Silicon:redstoneChipset:2> , <minecraft:dye:4>*2], false);
mods.buildcraft.AssemblyTable.addRecipe(<gendustry:EnvProcessor>, 150000, [<minecraft:diamond>*2, <gregtech:gt.multiitem.technological:30382> , <minecraft:dye:4>*2]);
recipes.remove(<gendustry:EnvProcessor>);
recipes.addShaped(<gendustry:EnvProcessor>, [[<ore:gemDiamond>, <ore:gemLapis>, <ore:gemDiamond>], [<ore:gemLapis>, <gregtech:gt.multiitem.technological:30382>, <ore:gemLapis>], [<ore:gemDiamond>, <ore:gemLapis>, <ore:gemDiamond>]]);

recipes.remove(<gendustry:GeneticsProcessor>);
recipes.addShaped(<gendustry:GeneticsProcessor>, [[<ore:gemDiamond>, <ore:gemQuartz>, <ore:gemDiamond>], [<ore:gemQuartz>, <gregtech:gt.multiitem.technological:30384>, <ore:gemQuartz>], [<ore:gemDiamond>, <ore:gemQuartz>, <ore:gemDiamond>]]);
mods.buildcraft.AssemblyTable.removeRecipe(<gendustry:GeneticsProcessor>, [<minecraft:diamond>*2, <BuildCraft|Silicon:redstoneChipset:4> , <minecraft:quartz>*2], false);
mods.buildcraft.AssemblyTable.addRecipe(<gendustry:GeneticsProcessor>, 150000, [<minecraft:diamond>*2, <gregtech:gt.multiitem.technological:30384> , <minecraft:quartz>*2]);

//DimDoors

recipes.remove(<dimdoors:Golden Door Item>);
recipes.remove(<dimdoors:Quartz Door Item>);
recipes.addShaped(<dimdoors:Golden Door Item>, [[<ore:plateGold>, <ore:plateGold>, null], [<ore:plateGold>, <ore:plateGold>, <ore:craftingToolHardHammer>], [<ore:plateGold>, <ore:plateGold>, null]]);
recipes.addShaped(<dimdoors:Quartz Door Item>, [[<ore:plateGemNetherQuartz>, <ore:plateGemNetherQuartz>], [<ore:plateGemNetherQuartz>, <ore:plateGemNetherQuartz>], [<ore:plateGemNetherQuartz>, <ore:plateGemNetherQuartz>]]);


