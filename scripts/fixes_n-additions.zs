recipes.addShapeless(<psychedelicraft:tobaccoSeeds>, [<ore:leafTobacco>]);
recipes.addShapeless(<psychedelicraft:cannabisSeeds>, [<ore:leafCannabis>]);
recipes.addShapeless(<psychedelicraft:hop_seeds>, [<ore:conesHops>]);
recipes.addShapeless(<psychedelicraft:cocaSeeds>, [<ore:leafCoca>]);

//OpenBlocks
//still needed, there are no original recipes for colored elevators
recipes.addShapeless(<OpenBlocks:elevator:15>, [<OpenBlocks:elevator>, <ore:dyeBlack>]);
recipes.addShapeless(<OpenBlocks:elevator:14>, [<OpenBlocks:elevator>, <ore:dyeRed>]);
recipes.addShapeless(<OpenBlocks:elevator:13>, [<OpenBlocks:elevator>, <ore:dyeGreen>]);
recipes.addShapeless(<OpenBlocks:elevator:12>, [<OpenBlocks:elevator>, <ore:dyeBrown>]);
recipes.addShapeless(<OpenBlocks:elevator:11>, [<OpenBlocks:elevator>, <ore:gemLapis>]);
recipes.addShapeless(<OpenBlocks:elevator:9>, [<OpenBlocks:elevator>, <ore:dyeCyan>]);
recipes.addShapeless(<OpenBlocks:elevator:8>, [<OpenBlocks:elevator>, <ore:dyeLightGray>]);
recipes.addShapeless(<OpenBlocks:elevator:7>, [<OpenBlocks:elevator>, <ore:dyeGray>]);
recipes.addShapeless(<OpenBlocks:elevator:6>, [<OpenBlocks:elevator>, <ore:dyePink>]);
recipes.addShapeless(<OpenBlocks:elevator:5>, [<OpenBlocks:elevator>, <ore:dyeLime>]);
recipes.addShapeless(<OpenBlocks:elevator:4>, [<OpenBlocks:elevator>, <ore:dyeYellow>]);
recipes.addShapeless(<OpenBlocks:elevator:3>, [<OpenBlocks:elevator>, <ore:dyeLightBlue>]);
recipes.addShapeless(<OpenBlocks:elevator:2>, [<OpenBlocks:elevator>, <ore:dyeMagenta>]);
recipes.addShapeless(<OpenBlocks:elevator:1>, [<OpenBlocks:elevator>, <ore:dyeOrange>]);
recipes.addShapeless(<OpenBlocks:elevator:10>, [<OpenBlocks:elevator>, <ore:dyePurple>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:15>, [<OpenBlocks:elevator:15>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:14>, [<OpenBlocks:elevator:14>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:13>, [<OpenBlocks:elevator:13>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:12>, [<OpenBlocks:elevator:12>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:11>, [<OpenBlocks:elevator:11>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:10>, [<OpenBlocks:elevator:10>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:9>, [<OpenBlocks:elevator:9>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:8>, [<OpenBlocks:elevator:8>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:7>, [<OpenBlocks:elevator:7>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:6>, [<OpenBlocks:elevator:6>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:5>, [<OpenBlocks:elevator:5>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:4>, [<OpenBlocks:elevator:4>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:3>, [<OpenBlocks:elevator:3>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:2>, [<OpenBlocks:elevator:2>, <ore:ingotIron>, <ore:ingotIron>]);
recipes.addShapeless(<OpenBlocks:elevator_rotating:1>, [<OpenBlocks:elevator:1>, <ore:ingotIron>, <ore:ingotIron>]);

//OpenComputers
//using a file instead of cutting wire
recipes.removeShapeless(<OpenComputers:item:111>);
recipes.addShapeless(<OpenComputers:item:111> * 6, [<ore:gemDiamond>, <ore:craftingToolFile>]);

//Project Red
//more fitting than 4 gold ingots and glass planes
recipes.remove(<ProjRed|Transportation:projectred.transportation.pipe:7>);
recipes.addShaped(<ProjRed|Transportation:projectred.transportation.pipe:7> * 8, [[<ore:chunkGtBrass>, <ore:glass>, <ore:chunkGtBrass>]]);

// StoneRod Fix
recipes.addShaped(<ForgeMicroblock:stoneRod> * 2, [[<ore:craftingToolSaw>, null], [null, <ore:stickLongStone>]]);

//creosote to bioethanol with Buildcraft
mods.buildcraft.Refinery.addRecipe(<liquid:bioethanol>, 20, 20, <liquid:creosote>, <liquid:creosote>);

//GT BC Compat
//InputFluid, rfPerTick, BurningTime in Ticks
//mods.buildcraft.Fuels.addCombustionEngineFuel(<liquid:diesel>, 60, 22);
//mods.buildcraft.Fuels.addCombustionEngineFuel(<liquid:diesel>, 60, 22);
//mods.buildcraft.Fuels.addCombustionEngineFuel(<liquid:diesel>, 60, 22);
//InputFluid, OutputCooling by mB
//mods.buildcraft.Fuels.addCombustionEngineCoolant(<liquid:oil>, 100);
//InputStack, OutputFluid
//64/6		384/L
//64/8		512/L
//60RF/25K	(375/L)1500RF/1L

//16/9		144/L
//16/12		192/L
//40RF/15K	(150/L)600RF/L

//64/5 		320/L
//64/7		448/L
//60RF/22K