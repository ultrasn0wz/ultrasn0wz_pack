val Silicon = <ore:itemSilicon>;
Silicon.add(<ProjRed|Core:projectred.core.part:12>);

val listAllseed = <ore:listAllseed>;
listAllseed.add(<plantmegapack:seedBeet>);
listAllseed.add(<plantmegapack:seedBellPepperOrange>);
listAllseed.add(<plantmegapack:seedBellPepperRed>);
listAllseed.add(<plantmegapack:seedBellPepperYellow>);
listAllseed.add(<plantmegapack:seedBroccoli>);
listAllseed.add(<plantmegapack:seedCassava>);
listAllseed.add(<plantmegapack:seedCelery>);
listAllseed.add(<plantmegapack:seedCorn>);
listAllseed.add(<plantmegapack:seedCucumber>);
listAllseed.add(<plantmegapack:seedEggplant>);
listAllseed.add(<plantmegapack:seedGreenBean>);
listAllseed.add(<plantmegapack:seedLeek>);
listAllseed.add(<plantmegapack:seedLettuce>);
listAllseed.add(<plantmegapack:seedOnion>);
listAllseed.add(<plantmegapack:seedSorrel>);
listAllseed.add(<plantmegapack:seedSpinach>);
listAllseed.add(<plantmegapack:seedTomato>);

val seedFlax = <ore:seedFlax>;
seedFlax.add(<atum:item.flaxSeeds>);

val cropFlax = <ore:cropFlax>;
cropFlax.add(<atum:item.flax>);

val seedEnderLily = <ore:seedEnderLily>;
seedEnderLily.add(<ExtraUtilities:plant/ender_lilly>);

val cropEnderLily = <ore:cropEnderLily>;
cropEnderLily.add(<EnderZoo:enderFragment>);

val Furnace = <ore:craftingFurnace>;
Furnace.add(<atum:tile.furnaceIdle>);

val crushedMeteoricIron = <ore:crushedMeteoricIron>;
crushedMeteoricIron.add(<GalacticraftCore:item.meteoricIronRaw>);

// PMP Fix

val foodBeet = <ore:foodBeet>;
foodBeet.add(<plantmegapack:foodBeet>);

val foodBellPepperOrange = <ore:foodBellPepperOrange>;
foodBellPepperOrange.add(<plantmegapack:foodBellPepperOrange>);

val foodBellPepperRed = <ore:foodBellPepperRed>;
foodBellPepperRed.add(<plantmegapack:foodBellPepperRed>);

val foodBellPepperYellow = <ore:foodBellPepperYellow>;
foodBellPepperYellow.add(<plantmegapack:foodBellPepperYellow>);

val foodBroccoli = <ore:foodBroccoli>;
foodBroccoli.add(<plantmegapack:foodBroccoli>);

val foodCassavaRoot = <ore:foodCassavaRoot>;
foodCassavaRoot.add(<plantmegapack:foodCassavaRoot>);

val foodCelery = <ore:foodCelery>;
foodCelery.add(<plantmegapack:foodCelery>);

val foodCentellaLeaves = <ore:foodCentellaLeaves>;
foodCentellaLeaves.add(<plantmegapack:foodCentellaLeaves>);

val foodCorn = <ore:foodCorn>;
foodCorn.add(<plantmegapack:foodCorn>);

val foodCucumber = <ore:foodCucumber>;
foodCucumber.add(<plantmegapack:foodCucumber>);

val foodEggplant = <ore:foodEggplant>;
foodEggplant.add(<plantmegapack:foodEggplant>);

val foodGreenBeans = <ore:foodGreenBeans>;
foodGreenBeans.add(<plantmegapack:foodGreenBeans>);

val foodLaksaLeaves = <ore:foodLaksaLeaves>;
foodLaksaLeaves.add(<plantmegapack:foodLaksaLeaves>);

val foodLeek = <ore:foodLeek>;
foodLeek.add(<plantmegapack:foodLeek>);

val foodLettuce = <ore:foodLettuce>;
foodLettuce.add(<plantmegapack:foodLettuce>);

val foodMozukuSeaweed = <ore:foodMozukuSeaweed>;
foodMozukuSeaweed.add(<plantmegapack:foodMozukuSeaweed>);

val foodOnion = <ore:foodOnion>;
foodOnion.add(<plantmegapack:foodOnion>);

val foodPeanuts = <ore:foodPeanuts>;
foodPeanuts.add(<plantmegapack:foodPeanuts>);

val foodPeanut = <ore:foodPeanut>;
foodPeanut.add(<plantmegapack:foodPeanuts>);

val foodPricklyPearFruit = <ore:foodPricklyPearFruit>;
foodPricklyPearFruit.add(<plantmegapack:foodPricklyPearFruit>);

val foodQuinoaSeeds = <ore:foodQuinoaSeeds>;
foodQuinoaSeeds.add(<plantmegapack:foodQuinoaSeeds>);

val foodRice = <ore:foodRice>;
foodRice.add(<plantmegapack:foodRice>);

val foodSacredLotusRoot = <ore:foodSacredLotusRoot>;
foodSacredLotusRoot.add(<plantmegapack:foodSacredLotusRoot>);

val foodSorrel = <ore:foodSorrel>;
foodSorrel.add(<plantmegapack:foodSorrel>);

val foodSpinach = <ore:foodSpinach>;
foodSpinach.add(<plantmegapack:foodSpinach>);

val foodTomato = <ore:foodTomato>;
foodTomato.add(<plantmegapack:foodTomato>);

val foodTaroRoot = <ore:foodTaroRoot>;
foodTaroRoot.add(<plantmegapack:foodTaroRoot>);

val foodWasabiStem = <ore:foodWasabiStem>;
foodWasabiStem.add(<plantmegapack:foodWasabiStem>);

val foodWaterChestnut = <ore:foodWaterChestnut>;
foodWaterChestnut.add(<plantmegapack:foodWaterChestnut>);

val foodWatercress = <ore:foodWatercress>;
foodWatercress.add(<plantmegapack:foodWatercress>);

val foodWildRice = <ore:foodWildRice>;
foodWildRice.add(<plantmegapack:foodWildRice>);


val cropBeet = <ore:cropBeet>;
cropBeet.add(<plantmegapack:foodBeet>);

val cropBellPepperOrange = <ore:cropBellPepperOrange>;
cropBellPepperOrange.add(<plantmegapack:foodBellPepperOrange>);

val cropBellPepperRed = <ore:cropBellPepperRed>;
cropBellPepperRed.add(<plantmegapack:foodBellPepperRed>);

val cropBellPepperYellow = <ore:cropBellPepperYellow>;
cropBellPepperYellow.add(<plantmegapack:foodBellPepperYellow>);

val cropBroccoli = <ore:cropBroccoli>;
cropBroccoli.add(<plantmegapack:foodBroccoli>);

val cropCassavaRoot = <ore:cropCassavaRoot>;
cropCassavaRoot.add(<plantmegapack:foodCassavaRoot>);

val cropCelery = <ore:cropCelery>;
cropCelery.add(<plantmegapack:foodCelery>);

val cropCentellaLeaves = <ore:cropCentellaLeaves>;
cropCentellaLeaves.add(<plantmegapack:foodCentellaLeaves>);

val cropCorn = <ore:cropCorn>;
cropCorn.add(<plantmegapack:foodCorn>);

val cropCucumber = <ore:cropCucumber>;
cropCucumber.add(<plantmegapack:foodCucumber>);

val cropEggplant = <ore:cropEggplant>;
cropEggplant.add(<plantmegapack:foodEggplant>);

val cropGreenBeans = <ore:cropGreenBeans>;
cropGreenBeans.add(<plantmegapack:foodGreenBeans>);

val cropLaksaLeaves = <ore:cropLaksaLeaves>;
cropLaksaLeaves.add(<plantmegapack:foodLaksaLeaves>);

val cropLeek = <ore:cropLeek>;
cropLeek.add(<plantmegapack:foodLeek>);

val cropLettuce = <ore:cropLettuce>;
cropLettuce.add(<plantmegapack:foodLettuce>);

val cropMozukuSeaweed = <ore:cropMozukuSeaweed>;
cropMozukuSeaweed.add(<plantmegapack:foodMozukuSeaweed>);

val cropOnion = <ore:cropOnion>;
cropOnion.add(<plantmegapack:foodOnion>);

val cropPeanut = <ore:cropPeanut>;
cropPeanut.add(<plantmegapack:foodPeanuts>);

val cropPricklyPearFruit = <ore:cropPricklyPearFruit>;
cropPricklyPearFruit.add(<plantmegapack:foodPricklyPearFruit>);

val cropQuinoaSeeds = <ore:cropQuinoaSeeds>;
cropQuinoaSeeds.add(<plantmegapack:foodQuinoaSeeds>);

val cropRice = <ore:cropRice>;
cropRice.add(<plantmegapack:foodRice>);
cropRice.add(<plantmegapack:foodWildRice>);

val cropSacredLotusRoot = <ore:cropSacredLotusRoot>;
cropSacredLotusRoot.add(<plantmegapack:foodSacredLotusRoot>);

val cropSorrel = <ore:cropSorrel>;
cropSorrel.add(<plantmegapack:foodSorrel>);

val cropSpinach = <ore:cropSpinach>;
cropSpinach.add(<plantmegapack:foodSpinach>);

val cropTomato = <ore:cropTomato>;
cropTomato.add(<plantmegapack:foodTomato>);

val cropTaroRoot = <ore:cropTaroRoot>;
cropTaroRoot.add(<plantmegapack:foodTaroRoot>);

val cropWasabiStem = <ore:cropWasabiStem>;
cropWasabiStem.add(<plantmegapack:foodWasabiStem>);

val cropChestnut = <ore:cropChestnut>;
cropChestnut.add(<plantmegapack:foodWaterChestnut>);

val cropWaterChestnut = <ore:cropWaterChestnut>;
cropWaterChestnut.add(<plantmegapack:foodWaterChestnut>);

val cropWatercress = <ore:cropWatercress>;
cropWatercress.add(<plantmegapack:foodWatercress>);

val cropWildRice = <ore:cropWildRice>;
cropWildRice.add(<plantmegapack:foodWildRice>);


val listAllberry = <ore:listAllberry>;
listAllberry.add(<plantmegapack:berriesBeauty>);
listAllberry.add(<plantmegapack:berriesBlack>);
listAllberry.add(<plantmegapack:berriesBlue>);
listAllberry.add(<plantmegapack:berriesElder>);
listAllberry.add(<plantmegapack:berriesGoose>);
listAllberry.add(<plantmegapack:berriesHarlequinMistletoe>);
listAllberry.add(<plantmegapack:berriesHuckle>);
listAllberry.add(<plantmegapack:berriesOrange>);
listAllberry.add(<plantmegapack:berriesSnow>);
listAllberry.add(<plantmegapack:berriesStraw>);

val cropBlackberry = <ore:cropBlackberry>;
cropBlackberry.add(<plantmegapack:berriesBlack>);

val cropBlueberry = <ore:cropBlueberry>;
cropBlueberry.add(<plantmegapack:berriesBlue>);

val cropElderberry = <ore:cropElderberry>;
cropElderberry.add(<plantmegapack:berriesElder>);

val cropGooseberry = <ore:cropGooseberry>;
cropGooseberry.add(<plantmegapack:berriesGoose>);

val cropStrawberry = <ore:cropStrawberry>;
cropStrawberry.add(<plantmegapack:berriesStraw>);

// Psychedelicraft
val treeLeaves = <ore:treeLeaves>;
treeLeaves.add(<psychedelicraft:psycheLeaves>);
